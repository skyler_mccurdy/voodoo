var gulp = require('gulp');
var browserify = require('browserify');
var stringify = require('stringify');
var source = require('vinyl-source-stream');
var watchify = require('watchify');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var karma = require('gulp-karma');

function distBundle(b){
	b.bundle()
		.pipe(source('index.js'))
		.pipe(buffer())
		.pipe(sourcemaps.init({loadMaps:true}))
		.pipe(buffer())
		.pipe(uglify())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./dist/'));
}

gulp.task('dist',function(){
	var b= browserify({
			cache: {},
			packageCache: {},
			fullPaths:true,
			debug:true
		});
	b = b.transform(stringify(['.html']));
	b = watchify(b);
	b.on('update',function(){
		distBundle(b);
	});
	
	
	b.add('./voodoo/index.js');
	distBundle(b);
		
});


gulp.task('default',['dist']);

