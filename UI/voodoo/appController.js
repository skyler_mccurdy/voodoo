(function(){

	appController.$inject = ['$scope','$timeout','Config'];
	angular.module('voodoo').controller('AppController',appController);

	function appController($scope,$timeout,Config){
	
		$scope.Config = Config;
	
		$scope.showSplashScreen = true;
		$timeout(function(){
			$scope.showSplashScreen=false;
		},1000);

	}
	
})();
