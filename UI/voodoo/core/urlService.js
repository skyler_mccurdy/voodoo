(function(){

	angular.module('voodoo.core').factory('urlService',urlService);
	
	
	urlService.$inject = ['$window'];
	
	function urlService($window){
		var service = {
			app:{
				baseUrl: '',
				origin: ''
			}, 
			
			modules: {},
			 
			registerUrl: registerURL
		};
		
		console.log($window.location);
		
		var loc = $window.location;
		service.app.baseUrl = loc.origin+loc.pathname;
		service.app.origin = loc.origin;
		
		
		function registerURL(moduleName,urlName,url){
			service.modules[moduleName] = service.modules[moduleName] || {};
			service.modules[moduleName][urlName] = url;
		}
		
		return service;
//		return {};
	}

})();
